var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    autoprefixer    = require('gulp-autoprefixer'),
    concat          = require('gulp-concat'),
    cheerio         = require('gulp-cheerio'),
    path            = require('path'),
    browserSync     = require('browser-sync').create(),
    //cssnano       = require('gulp-cssnano'),
    //uglify        = require('gulp-uglify'),
    reload          = browserSync.reload;


gulp.task('build-css', function () {
    return gulp.src('sass/*')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css/'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: true
        }))
        //.pipe(cssnano())
        .pipe(browserSync.stream());
});


gulp.task('build-js-footer', function () {
    return gulp.src([
        'js-dev/js-init.js',
        'js-dev/plugins/**/*.js',
        'js-dev/global.js',
        'js-dev/slider.js',
        'js-dev/methods/**/*.js'
    ])
      .pipe(concat('script.js'))
      //.pipe(uglify())
      .pipe(gulp.dest('js/'))
      .pipe(browserSync.stream());
});


gulp.task('watch', function(){
    browserSync.init({
        proxy: 'localhost/js-school/slider',
        notify: false,
    });
    gulp.watch('template/**/*.php').on('change', reload);
    gulp.watch('sass/*.scss', ['build-css']);
    gulp.watch('sass/*/*.scss', ['build-css']);
    gulp.watch('js-dev/**/*.js', ['build-js-footer']);
});