<?php Site::getHeader(); ?>

<main role="main">
	
<div class="slider-wrapper">
	<div class="slider-wrapper__slides">
		<div class="slide slide-active"><img src="img/slide-1.jpg" alt="slide 1"></div>
		<div class="slide"><img src="img/slide-2.jpg" alt="slide 2"></div>
		<div class="slide"><img src="img/slide-3.jpg" alt="slide 3"></div>
		<div class="slide"><img src="img/slide-4.jpg" alt="slide 4"></div>
	</div>

	<div class="slider__controls">
		<button class="prev-slide">Prev</button>
		<button class="next-slide">Next</button>
	</div>
</div>

</main>

<?php Site::getFooter(); ?>