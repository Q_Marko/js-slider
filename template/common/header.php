<!DOCTYPE html>
<!--[if IE 8 ]><html lang="hr" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="hr" class="no-js ie9"> <![endif]-->
<html lang="hr">

<head>
	<meta charset="utf-8">
	<title>Slider js</title>	

	<link id="style" href="<?php echo Site::url(); ?>/css/style.css" rel="stylesheet" type="text/css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script defer src="<?php echo Site::url(); ?>/js/script.js"></script>

</head>

<body class="<?php echo Site::$bodyClass; ?>">
