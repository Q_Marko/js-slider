
/*Slider Modulo pattern */

var slider = (function () {

	var prevSlide = document.querySelector('.prev-slide');
	var nextSlide = document.querySelector('.next-slide');

	var sliderWrapper = document.getElementsByClassName('slider-wrapper__slides');
	var slideCount = document.getElementsByClassName('slide').length;
	var activeSlide = document.getElementsByClassName('slide-active');
	var firstSlide = sliderWrapper[0].firstElementChild;
	var lastSlide = sliderWrapper[0].lastElementChild;
	var slideNumber = 1;

	var _sliderCommands = {
		changePrev: prevSlide.onclick = function() {

			var prevActive = activeSlide[0].previousElementSibling;

			slideNumber--;

			if (slideNumber < 1) {
				firstSlide.classList.remove('slide-active');
				lastSlide.classList.add('slide-active');

				slideNumber = slideCount;
			} else {
				[].forEach.call(activeSlide, function(el) {
			    	el.classList.remove('slide-active');
				})

				prevActive.classList.add('slide-active');
			}
		
		},
		changeNext: nextSlide.onclick = function () {
		
			var nextActive = activeSlide[0].nextElementSibling;

			slideNumber++;

			if (slideNumber > slideCount) {
				lastSlide.classList.remove('slide-active');
				firstSlide.classList.add('slide-active');

				slideNumber = 1;
			} else {
				[].forEach.call(activeSlide, function(el) {
			    	el.classList.remove('slide-active');
				})

				nextActive.classList.add('slide-active');
			}
				
		}
	};

	return {

		sliderInit: function(parameters) {
			if ( parameters.changePrev ) {
				_sliderCommands.changePrev();
			}
			if ( parameters.changeNext ) {
				_sliderCommands.changeNext();
			}
		}

	};

})();

slider.sliderInit({
	changeNext: true,
	changePrev: true
});
